//
//  GKHackerNewsApiPlugin.h
//  KMCCordovaApp
//
//  Created by Kwan Cheng on 2/25/17.
//
//

#import <Cordova/CDVPlugin.h>
#import <GKHackerNewsApi/GKHackerNewsApi.h>

@interface GKHackerNewsApiPlugin : CDVPlugin<GKHackerNewsDelegate>
-(void) initSDK:(CDVInvokedUrlCommand*)command;
-(void) showHackerNews:(CDVInvokedUrlCommand*)command;
@end
