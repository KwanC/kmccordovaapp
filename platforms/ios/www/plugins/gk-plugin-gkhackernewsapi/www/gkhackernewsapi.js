cordova.define("gk-plugin-gkhackernewsapi.GKHackerNewsApi", function(require, exports, module) {
var cordova = require('cordova'), exec = require('cordova/exec');

var GKHackerNews = function() {
   this.channels = {
      topStoriesListAvailable : cordova.addWindowEventHandler("topStoriesListAvailable")
   };
   
   for (var key in this.channels) {
      this.channels[key].onHasSubscribersChange = GKHackerNews.onHasSubscribersChange;
   }
};
 
GKHackerNews.onHasSubscribersChange = function() {
  console.log("GKHackerNewsPlugin onHasSubscribersChange");
};
   
GKHackerNews.prototype._status = function(info) {
   cordova.fireWindowEvent("topStoriesListAvailable", {});
};

GKHackerNews.prototype._error = function(e) {
   console.log("Error initializing GKHackerNews: " + e);
};
           
GKHackerNews.prototype.initSDK = function(e) {
   exec(gkHackerNews._status, gkHackerNews._error, "GKHackerNewsApiPlugin", "initSDK", {});
};
           
GKHackerNews.prototype.showHackerNews = function() {
    exec(null,null, "GKHackerNewsApiPlugin", "showHackerNews", {});
};

var gkHackerNews = new GKHackerNews();

module.exports = gkHackerNews;
});
