cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "gk-plugin-gkhackernewsapi.GKHackerNewsApi",
        "file": "plugins/gk-plugin-gkhackernewsapi/www/gkhackernewsapi.js",
        "pluginId": "gk-plugin-gkhackernewsapi",
        "clobbers": [
            "gk.hackernewsapi"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.1",
    "gk-plugin-gkhackernewsapi": "1.0.0"
};
// BOTTOM OF METADATA
});